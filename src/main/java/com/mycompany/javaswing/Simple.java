/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.javaswing;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author lenovo
 */
public class Simple {

    JFrame f;

    Simple() {
        f = new JFrame(); //การสร้างอินสแตนซ์ของ JFrame

        JButton b = new JButton("click"); //การสร้างอินสแตนซ์ของ JButton
        b.setBounds(130, 100, 100, 40);

        f.add(b); //เพิ่มปุ่มใน JFrame

        f.setSize(400, 500); //set ความกว้าง, ความสูง
        f.setLayout(null); //ไม่ต้องใช้ตัวจัดการเลย์เอาต์
        f.setVisible(true); //ทำให้มองเห็น Frame ได้
    }

    public static void main(String[] args) {
        new Simple();
    }

}
